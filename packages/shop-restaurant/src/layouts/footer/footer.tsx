import React from 'react';
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
} from './footer.style';
import { GET_INFO_SHOP } from 'utils/graphql/query/infoshop.query'; 
import { useQuery, gql } from '@apollo/client';
import config from 'setting/config';


export const Footer = ( ) => {


  const { data, error, refetch, fetchMore } = useQuery(GET_INFO_SHOP,
    {
        variables: {
          clientid: config().SUBSCRIPTION_ID
        }
    }); 

  return (
    <Box>
      
      <Container>
        <Row>
          <Column>
            <Heading>{data && data.info_shop_view[0].site_name}</Heading>
            <FooterLink href="#">Facebook</FooterLink>
            <FooterLink href="#">Instagram</FooterLink>
          </Column>
          <Column>
            <Heading>Acerca de {data && data.info_shop_view[0].site_name}</Heading>
            <FooterLink href="#">Lee nuestro blog</FooterLink>
            <FooterLink href="#">Seguir mi pedido</FooterLink>
            <FooterLink href="#">Chat</FooterLink>
            <FooterLink href="#">Conoce nuestros locales</FooterLink>
          </Column>
          <Column>
            <Heading>Información</Heading>
            <FooterLink href="#">Politicas de privacidad</FooterLink>
            <FooterLink href="#">Terminos y condiciones</FooterLink>
            <FooterLink href="#">Ayuda</FooterLink>
            <FooterLink href="#">Contacto</FooterLink>
          </Column>
       
        </Row>
      </Container>
      
      <h6 style={{ color: 'gray', textAlign: 'center', padding: '20px 10px', borderTop: '1px solid #009E7F' }}>
      {data && data.info_shop_view[0].site_name} | Por Tu-ecommerce.cl
      </h6>
     
    </Box>
    
  );
};
export default Footer;